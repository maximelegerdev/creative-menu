import { MenuItem, MenuItemProps } from './components/MenuItem';
import './style/App.scss';

import imageTacoBell from './assets/img/cb_cg_home_ultrawide_red_thumb.jpg';
import imageReebok from './assets/img/cb_bmh_home_ultrawide_red_thumb.jpg';
import imageNike from './assets/img/cb_ls_home_ultrawide_red_thumb.jpg';
import imageUniversal from './assets/img/cb_cws_home_ultrawide_red_thumb.jpg';
import imagePerrierJouet from './assets/img/cb_leab_home_ultrawide_red_thumb.jpg';
import { useEffect, useRef } from 'react';

const menuItems: MenuItemProps[] = [
  {
    number: '01',
    title: 'Chasing Gold',
    description: 'Integrated',
    brand: 'Taco Bell',
    background: imageTacoBell,
  },
  {
    number: '02',
    title: 'Be More Human',
    description: 'Brand',
    brand: 'Reebok',
    background: imageReebok,
  },
  {
    number: '03',
    title: 'Legacy Summit',
    description: 'Activation',
    brand: 'Nike',
    background: imageNike,
  },
  {
    number: '04',
    title: 'Creative We Stand',
    description: 'Integrated',
    brand: 'Universal',
    background: imageUniversal,
  },
  {
    number: '05',
    title: "L'Eden, Art Basel",
    description: 'Activation',
    brand: 'Perrier-Jouët',
    background: imagePerrierJouet,
  },
];

type TitleProps = {
  info: string;
  person: string;
  link: string;
};

const titles: TitleProps[] = [
  {
    info: 'Developed by',
    person: 'Maxime "June" Léger',
    link: 'https://gitlab.com/maximelegerdev',
  },
  {
    info: 'Original website by',
    person: 'Victor Work',
    link: 'https://victor.work/',
  },
  {
    info: 'Course by',
    person: 'Benjamin Code',
    link: 'https://shop.benjamincode.tv',
  },
];

const Title: React.FC<TitleProps> = ({ info, person, link }) => {
  const titleRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const title = titleRef.current;
    if (!title) {
      return;
    }

    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            entry.target.classList.add('visible');
          } else {
            entry.target.classList.remove('visible');
          }
        });
      },
      { root: null, rootMargin: '0px 0px -200px 0px' },
    );

    observer.observe(title);

    return () => {
      observer.disconnect();
    };
  }, []);

  return (
    <div
      className="title"
      ref={titleRef}
    >
      <span>{info}</span>
      <a
        href={link}
        target="_blank"
        rel="noopener noreferrer"
      >
        {person}
      </a>
    </div>
  );
};

function App() {
  return (
    <div className="app">
      <div className="titles">
        {titles.map((title) => {
          return (
            <Title
              info={title.info}
              person={title.person}
              link={title.link}
            ></Title>
          );
        })}
      </div>
      {menuItems.map((menuItem) => {
        return (
          <MenuItem
            number={menuItem.number}
            title={menuItem.title}
            description={menuItem.description}
            brand={menuItem.brand}
            background={menuItem.background}
          />
        );
      })}
    </div>
  );
}

export default App;
