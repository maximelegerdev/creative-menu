import { useEffect, useRef } from "react";
import "../style/MenuItem.scss";

export type MenuItemProps = {
  number: string;
  title: string;
  description: string;
  brand: string;
  background: string;
};

export const MenuItem: React.FC<MenuItemProps> = ({
  number,
  title,
  description,
  brand,
  background,
}) => {
  const itemRef = useRef<HTMLDivElement>(null);
  const itemContentRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const item = itemRef.current;
    const itemContent = itemContentRef.current;
    if (!item || !itemContent) {
      return;
    }

    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            entry.target.classList.add("visible");
          } else {
            entry.target.classList.remove("visible");
          }
        });
      },
      { root: null, rootMargin: "0px 0px -200px 0px" },
    );

    observer.observe(item);
    observer.observe(itemContent);

    return () => {
      observer.disconnect();
    };
  }, []);

  return (
    <div
      className="item"
      ref={itemRef}
    >
      <div
        className="item__background"
        style={{ backgroundImage: `url(${background})` }}
      />
      <div
        className="item__content"
        ref={itemContentRef}
      >
        <span className="number">{number}</span>
        <h2>{title}</h2>
        <div className="item__metas">
          <span className="description">{description}</span>
          <p>{brand}</p>
        </div>
      </div>
    </div>
  );
};
