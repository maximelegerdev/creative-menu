# Creative Menu

A beautiful, creative menu full of animations, that taught me more about animating in CSS and TS in general.

[Get the course by BenjaminCode](https://shop.benjamincode.tv/products/magnifique-menu-interactif-avec-html-css-javascript-et-gitcopilot)

[Portfolio of Victor Work, developer of the original website](https://victor.work/)
